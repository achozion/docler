<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Util\Exception\XmlParsingException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AppletLanguageXmlFilesGenerator
{
    private $logger;

    private $params;

    private $translator;

    public function __construct(LoggerInterface $logger, ParameterBagInterface $params, TranslatorInterface $translator)
    {
        $this->logger = $logger;
        $this->params = $params;
        $this->translator = $translator;
    }

    /*
     * @return boolean
     */
    public function generate()
    {
        $systemTranslatedApplications = $this->params->get('parameterSystemTranslatedApplications');

        foreach ($systemTranslatedApplications[0] as $portal => $languages) {
            foreach ($languages as $key => $thisLang) {
                $this->createXML($thisLang);
            }
        }

        $this->logger->info('Applet Language Xmls Successfully generate. Done');

        return true;
    }

    protected function createXML($thisLang): void
    {
        $parameterXmlName = $this->params->get('parameterXmlLang') . strtolower($thisLang);
        $parameterXmlExtension = $this->params->get('parameterXmlExtension');
        $parameterFlashFilePath = __DIR__. $this->params->get('parameterFlashFilePath');
        $parameterXmlDataString = $this->params->get('parameterXmlDataString');
        $parameterXmlStartString = $this->params->get('parameterXmlStartString');
        $parameterMessages = $this->params->get('parameterMessages');
        $forcedLocale = strtolower($thisLang) . "_" . strtoupper($thisLang);
        $fileName = $parameterXmlName . $parameterXmlExtension;

        try {
            if (file_exists($parameterFlashFilePath . $fileName)) {
                $xml = simplexml_load_file($parameterFlashFilePath . $fileName);
            } else {
                $fs = new Filesystem();
                $fs->mkdir($parameterFlashFilePath);
                $xml = new \SimpleXMLElement($parameterXmlStartString . $parameterXmlDataString);
            }

            $xml->addChild( $this->params->get('button_go_private'), $this->translator->trans($this->params->get('button_go_private'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_send'), $this->translator->trans($this->params->get('button_send'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_confirm'), $this->translator->trans($this->params->get('button_confirm'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_ok'), $this->translator->trans($this->params->get('button_ok'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_close'), $this->translator->trans($this->params->get('button_close'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_close_private'), $this->translator->trans($this->params->get('button_close_private'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_back_to_chat'), $this->translator->trans($this->params->get('button_back_to_chat'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_back_to_cam'), $this->translator->trans($this->params->get('button_back_to_cam'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('button_back'), $this->translator->trans($this->params->get('button_back'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('cancel'), $this->translator->trans($this->params->get('cancel'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_surprise'), $this->translator->trans($this->params->get('info_surprise'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_gallery'), $this->translator->trans($this->params->get('info_gallery'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_gallery_no_image'), "{@performerid}" . $this->translator->trans($this->params->get('info_gallery_no_image'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_snapshot'), $this->translator->trans($this->params->get('info_snapshot'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_add_fav'), $this->translator->trans($this->params->get('info_add_fav'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_rem_fav'), $this->translator->trans($this->params->get('info_rem_fav'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_added_fav'), $this->translator->trans($this->params->get('info_added_fav'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_removed_fav'), $this->translator->trans($this->params->get('info_removed_fav'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_buycredit'), $this->translator->trans($this->params->get('info_buycredit'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_camera_on'), $this->translator->trans($this->params->get('info_camera_on'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_camera_off'), $this->translator->trans($this->params->get('info_camera_off'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_size'), $this->translator->trans($this->params->get('info_size'), [], $parameterMessages, $forcedLocale) );
            $xml->addChild( $this->params->get('info_personalinfo'), $this->translator->trans($this->params->get('info_personalinfo'), [], $parameterMessages, $forcedLocale) );

            $xml->asXML($parameterFlashFilePath . $fileName);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            Throw new XmlParsingException();
        }

    }


}