<?php

namespace App\Tests;

use App\Service\AppletLanguageXmlFilesGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class CommandGeneratesTest extends KernelTestCase
{

    protected function setUp(): void
    {
        static::bootKernel();

        `rm -rf var/cache/*`;
        `rm -rf var/sessions/*`;
        `php bin/console cache:warmup --env=test`;
    }

    public function testIsXmlValid()
    {
        $systemTranslatedApplications = static::$kernel->getContainer()->getParameter('parameterSystemTranslatedApplications');

        foreach ($systemTranslatedApplications[0] as $portal => $languages) {
            foreach ($languages as $key => $thisLang) {


                $parameterXmlName = static::$kernel->getContainer()->getParameter('parameterXmlLang') . strtolower($thisLang);
                $parameterXmlExtension = static::$kernel->getContainer()->getParameter('parameterXmlExtension');
                $parameterFlashFilePath = __DIR__ . "/../public/flash/";
                $fileName = $parameterXmlName . $parameterXmlExtension;

                if (file_exists($parameterFlashFilePath . $fileName)) {
                    $xml = simplexml_load_file($parameterFlashFilePath . $fileName);
                }

                $this->assertInstanceOf(\SimpleXMLElement::class, $xml);
                $this->assertNotFalse($xml);
            }
        }
    }

    public function testXmlAttributes()
    {
        $systemTranslatedApplications = static::$kernel->getContainer()->getParameter('parameterSystemTranslatedApplications');

        foreach ($systemTranslatedApplications[0] as $portal => $languages) {
            foreach ($languages as $key => $thisLang) {

                $parameterXmlName = static::$kernel->getContainer()->getParameter('parameterXmlLang') . strtolower($thisLang);
                $parameterXmlExtension = static::$kernel->getContainer()->getParameter('parameterXmlExtension');
                $parameterFlashFilePath = __DIR__ . "/../public/flash/";
                $fileName = $parameterXmlName . $parameterXmlExtension;

                if (file_exists($parameterFlashFilePath . $fileName)) {
                    $xml = simplexml_load_file($parameterFlashFilePath . $fileName);
                }

                $this->assertObjectHasAttribute(static::$kernel->getContainer()->getParameter('button_go_private'), $xml);
            }
        }

    }

}