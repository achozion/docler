Mellékeltem a szakmai leirást is:

Megoldásom Symfony 4.4 LTS verzióval.

két féle módon lehet meghívni az API -t:
- routing ból : (default rooting ( / ) )
- command ból :
    - php bin/console app:generate-language-file 
    - php bin/console app:generate-applet-language-xml-file

A rendszer legenerálja a lang fájlokat és az Applet language XML fájlokat ide:
fladh:
- public/flash/lang_en.xml
- public/flash/lang_hu.xml

portal:
- public/portal/en.php
- public/portal/hu.php

Csomagkezelő:
composer

Unit test:
PHP Unit és Bridge

tests:
- test command run
- test xml 
 futtatása:
php bin/phpunit

Docker konténer szolgáltatja a környezetet
docker compose fájl mellékeve

docker up:
docker-compose up -d
docker login / enter docker:
 docker exec -it icftech-mysql mysql -usf4_user -psf4_pw

oldal elérése böngészőből a 8000 es porton történik:
http://localhost:8000/


Köszönöm a lehetőséget, hogy megoldhattam a tesztfeladatot.
Remélem a követelményeknek maradéktalanul megfeleltem.

Üdvözlettel:Varga Ákos
achozion@gmail.com


Feladatkiírás:

Task:

Your task is to refactor the LanguageBatchBo!

The solution will be evaluated based on the following goals:

    Keep the original functionality.
    Increase the inner code quality.
    Increase test coverage with unit tests.

Rules:

    Create local git repository for the project.
    Commit after each coding step, when the system is in working condition.
    The interface of the LanguageBatchBo can't be changed (the generate_language_files.php should remain the same), but (of course) it's content can change and it can be split into new classes.
    The ApiCall, and Config classes are mock/simplified versions of the original dependencies, they can not be changed.
    The error message of the exceptions can be simplified.
    The console output of the script doesn't have to be the same as in the original version.
    You can upgrade code and dependencies to PHP 7.
    Use PHPUnit as testing framework.
    Inline comments are not necessary.
    You can clone this repository, but the homework should be sent to us via email (including the git files) and it should not be shared on github, or otherwise on the internet.
