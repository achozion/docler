<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\LanguageFilesGenerator;

class GenerateLanguageFileCommand extends Command
{
    protected static $defaultName = 'app:generate-language-file';

    private $logger;

    private $languageFilesGenerator;

    public function __construct(LoggerInterface $logger, LanguageFilesGenerator $languageFilesGenerator)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->languageFilesGenerator = $languageFilesGenerator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate Language File.')
            ->setHelp('This command allows you to generate language file...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Generate Language File',
            '============',
            '',
        ]);

        $this->languageFilesGenerator->generate();

        $output->write('You are about to ');
        $output->write('generate language file.');

        return 0;
    }
}