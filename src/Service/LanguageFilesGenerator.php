<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\Translation\TranslatorInterface;

class LanguageFilesGenerator
{
    private $logger;

    private $params;

    private $translator;

    private $filesystem;

    public function __construct(LoggerInterface $logger, ParameterBagInterface $params, TranslatorInterface $translator, Filesystem $filesystem)
    {
        $this->logger = $logger;
        $this->params = $params;
        $this->translator = $translator;
        $this->filesystem = $filesystem;
    }

    /*
     * return boolean
     */
    public function generate()
    {
        $systemTranslatedApplications = $this->params->get('parameterSystemTranslatedApplications');

        foreach ($systemTranslatedApplications[0] as $portal => $languages) {
            foreach ($languages as $key => $thisLang) {
                $this->createFile($thisLang);
            }
        }

        $this->logger->info('Language files successfully generated. Done');

        return true;
    }

    protected function createFile($thisLang)
    {
        $parameterPortalFilePath = __DIR__. $this->params->get('parameterPortalFilePath');
        $parameterPhpExtension = $this->params->get('parameterPhpExtension');
        $parameterMessages = $this->params->get('parameterMessages');
        $forcedLocale = strtolower($thisLang) . "_" . strtoupper($thisLang);
        $thisPathAndFile = $parameterPortalFilePath . strtolower($thisLang) . $parameterPhpExtension;

        try {
            if (!$this->filesystem->exists($parameterPortalFilePath)) {
                $this->filesystem->mkdir($parameterPortalFilePath, 0777);
            }

            $this->filesystem->touch( $thisPathAndFile);
            $this->filesystem->dumpFile($thisPathAndFile, $this->params->get('parameterPhpStartString') . " \n");
            $this->filesystem->appendToFile($thisPathAndFile, $this->params->get('parameterArrayStartString') . " \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('our models')  . "\" => \"" .  $this->translator->trans($this->params->get('our models'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('favorites')  . "\" => \"" .  $this->translator->trans($this->params->get('favorites'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('help')  . "\" => \"" .  $this->translator->trans($this->params->get('help'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('account balance')  . "\" => \"" .  $this->translator->trans($this->params->get('account balance'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('member login')  . "\" => \"" .  $this->translator->trans($this->params->get('member login'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('login')  . "\" => \"" .  $this->translator->trans($this->params->get('login'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('sign up')  . "\" => \"" .  $this->translator->trans($this->params->get('sign up'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('get free credits')  . "\" => \"" .  $this->translator->trans($this->params->get('get free credits'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('boy')  . "\" => \"" .  $this->translator->trans($this->params->get('boy'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('tranny')  . "\" => \"" .  $this->translator->trans($this->params->get('tranny'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('transvestite')  . "\" => \"" .  $this->translator->trans($this->params->get('transvestite'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('couple')  . "\" => \"" .  $this->translator->trans($this->params->get('couple'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('group')  . "\" => \"" .  $this->translator->trans($this->params->get('group'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('girl')  . "\" => \"" .  $this->translator->trans($this->params->get('girl'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('hermaphrodite')  . "\" => \"" .  $this->translator->trans($this->params->get('hermaphrodite'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('shemale')  . "\" => \"" .  $this->translator->trans($this->params->get('shemale'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('all')  . "\" => \"" .  $this->translator->trans($this->params->get('all'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('18-22')  . "\" => \"" .  $this->translator->trans($this->params->get('18-22'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('asian')  . "\" => \"" .  $this->translator->trans($this->params->get('asian'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('big tits')  . "\" => \"" .  $this->translator->trans($this->params->get('big tits'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('blonde')  . "\" => \"" .  $this->translator->trans($this->params->get('blonde'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('ebony')  . "\" => \"" .  $this->translator->trans($this->params->get('ebony'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('interracial')  . "\" => \"" .  $this->translator->trans($this->params->get('interracial'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('latin')  . "\" => \"" .  $this->translator->trans($this->params->get('latin'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('latex')  . "\" => \"" .  $this->translator->trans($this->params->get('latex'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('leather')  . "\" => \"" .  $this->translator->trans($this->params->get('leather'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('mommy')  . "\" => \"" .  $this->translator->trans($this->params->get('mommy'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('muscular')  . "\" => \"" .  $this->translator->trans($this->params->get('muscular'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('skinny')  . "\" => \"" .  $this->translator->trans($this->params->get('skinny'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('small tits')  . "\" => \"" .  $this->translator->trans($this->params->get('small tits'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('white')  . "\" => \"" .  $this->translator->trans($this->params->get('white'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('man')  . "\" => \"" .  $this->translator->trans($this->params->get('man'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('anal')  . "\" => \"" .  $this->translator->trans($this->params->get('anal'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, "\"" . $this->params->get('woman')  . "\" => \"" .  $this->translator->trans($this->params->get('woman'), [], $parameterMessages, $forcedLocale) . "\", \n");
            $this->filesystem->appendToFile($thisPathAndFile, $this->params->get('parameterArrayEndString') . " \n");

        } catch (IOExceptionInterface $exception) {
            $this->logger->info('About to find a happy message!');
            echo "An error occurred while creating your directory at ".$exception->getPath();
        }
    }
}