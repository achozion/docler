<?php

namespace App\Controller\Api;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Service\LanguageFilesGenerator;
use App\Service\AppletLanguageXmlFilesGenerator;

class ApiCallController
{

    private $logger;
    private $languageFilesGenerator;
    private $appletLanguageXmlFilesGenerator;

    public function __construct(LoggerInterface $logger, LanguageFilesGenerator $languageFilesGenerator, AppletLanguageXmlFilesGenerator $appletLanguageXmlFilesGenerator)
    {
        $this->logger = $logger;
        $this->languageFilesGenerator = $languageFilesGenerator;
        $this->appletLanguageXmlFilesGenerator = $appletLanguageXmlFilesGenerator;
    }

    public function call()
    {
        $this->generateNewLanguageFiles();
        $this->generateNewAppletLanguageXmlFiles();

        $this->logger->info('All generation success. Done');

        return new Response(
            '<html><body>Files successfully generated</body></html>'
        );
    }

    public function generateNewLanguageFiles(): void
    {
        $message = $this->languageFilesGenerator->generate();
    }

    public function generateNewAppletLanguageXmlFiles(): void
    {
        $message = $this->appletLanguageXmlFilesGenerator->generate();
    }
}