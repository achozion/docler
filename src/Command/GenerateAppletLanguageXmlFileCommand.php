<?php

namespace App\Command;

use App\Service\AppletLanguageXmlFilesGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAppletLanguageXmlFileCommand extends Command
{
    protected static $defaultName = 'app:generate-applet-language-xml-file';

    private $logger;

    private $appletLanguageXmlFilesGenerator;

    public function __construct(LoggerInterface $logger, AppletLanguageXmlFilesGenerator $appletLanguageXmlFilesGenerator)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->appletLanguageXmlFilesGenerator = $appletLanguageXmlFilesGenerator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate Applet Language XML File.')
            ->setHelp('This command allows you to generate Applet language XML file...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Generate Applet Language XML File',
            '============',
            '',
        ]);

        $this->appletLanguageXmlFilesGenerator->generate();

        $output->write('You are about to ');
        $output->write('generate Applet language XML file.');

        return 0;
    }
}